from django.conf.urls import url
from foodsys.views import ProductView, Menu01View, Menu02View, Menu03View, Menu04View

urlpatterns = [
    url('product/', ProductView.as_view(), name='product'),
    url('menu01/', Menu01View.as_view(), name='menu01'),
    url('menu02/', Menu02View.as_view(), name='menu02'),
    url('menu03/', Menu03View.as_view(), name='menu03'),
    url('menu04/', Menu04View.as_view(), name='menu04'),
]
