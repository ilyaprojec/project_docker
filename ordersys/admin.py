from django.contrib import admin
from .models import ProductInCart, Order, ProductInOrder, ProductStatistic


class ProductInOrderInline(admin.TabularInline):
    model = ProductInOrder
    extra = 0


class ProductInCartAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ProductInCart._meta.fields]

    class Meta:
        model = ProductInCart


class ProductInOrderAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ProductInOrder._meta.fields]

    class Meta:
        model = ProductInOrder


class OrderAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Order._meta.fields]
    inlines = [ProductInOrderInline]
    list_filter = ('status',)

    class Meta:
        model = Order


class ProductStatisticAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ProductStatistic._meta.fields]

    class Meta:
        model = ProductStatistic


admin.site.register(ProductInCart, ProductInCartAdmin)
admin.site.register(ProductInOrder, ProductInOrderAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(ProductStatistic, ProductStatisticAdmin)
