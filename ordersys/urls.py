from django.conf.urls import url
from ordersys.views import cart_adding, CartView, checkout, payed_online, card, thank

urlpatterns = [
    url('cart/', CartView.as_view(), name='cart'),
    url('cart_adding/', cart_adding, name='cart_adding'),
    url('checkout/', checkout, name='checkout'),
    url('payed-online-order/', payed_online, name='payed_online'),
    url('card/', card, name='card'),
    url('thank/', thank, name='thank')
]
