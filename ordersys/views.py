import stripe
from decimal import Decimal
from django.conf import settings
from django.shortcuts import render
from django.contrib.auth import get_user_model
from django.views.generic.base import TemplateView
from django.shortcuts import redirect
from django.http import JsonResponse, Http404
from .models import ProductInOrder, ProductInCart, Order
from loginsys.forms import OrderingContactForm
from foodsys.models import Product


def checkout(request):
    if request.user.is_authenticated:
        products_in_cart = ProductInCart.objects.filter(user_id=get_user_model().objects.get(
            username=request.user.username).id, is_active=True, order__isnull=True)
    else:
        products_in_cart = ProductInCart.objects.filter(session_key=request.session.session_key, is_active=True, order__isnull=True)
    if not products_in_cart:
        raise Http404

    radio_cash = "cashRadio"
    radio_bonuses = "bonusesRadio"
    radio_card = "cardRadio"

    args = {}
    final_price = 0
    user = request.user
    form = OrderingContactForm(request.POST or None, user=request.user)

    for product in products_in_cart:
        final_price += product.total_price

    # CASH LOGIC
    def logic_cash():
        order = Order.objects.create(user=customer, customer_name=data["username"],
                                     customer_phone=data["phone"],
                                     customer_address=data["address"], status=Order.STATUS_IN_WORK,
                                     payment_method=Order.METHOD_CASH,
                                     comment=data["comment"])

        for name, value in data.items():
            if name.startswith("product_in_cart_"):
                product_in_cart_id = name.split("product_in_cart_")[1]
                product_in_cart = ProductInCart.objects.get(id=product_in_cart_id)
                product_in_cart.nmb = value
                product_in_cart.order = order
                product_in_cart.save(force_update=True)
                ProductInOrder.objects.create(product=product_in_cart.product, nmb=product_in_cart.nmb,
                                              price_per_item=product_in_cart.price_per_item,
                                              total_price=product_in_cart.total_price,
                                              order=order)

        if request.user.is_authenticated:
            ProductInCart.objects.filter(user_id=customer.id).delete()
        else:
            ProductInCart.objects.filter(session_key=request.session.session_key).delete()

        def delete_customer():
            if not request.user.is_authenticated:
                customer.delete()
        delete_customer()

    # BONUSES LOGIC
    def logic_bonuses():
        if customer.bonuses < final_price:
            return "not enough bonuses"
        else:
            customer.bonuses -= final_price
            customer.save(force_update=True)

        order = Order.objects.create(user=customer, customer_name=data["username"],
                                     customer_phone=data["phone"],
                                     customer_address=data["address"], status=Order.STATUS_PAYED,
                                     payment_method=Order.METHOD_BONUSES,
                                     comment=data["comment"])

        for name, value in data.items():
            if name.startswith("product_in_cart_"):
                product_in_cart_id = name.split("product_in_cart_")[1]
                product_in_cart = ProductInCart.objects.get(id=product_in_cart_id)
                product_in_cart.nmb = value
                product_in_cart.order = order
                product_in_cart.save(force_update=True)
                ProductInOrder.objects.create(product=product_in_cart.product, nmb=product_in_cart.nmb,
                                              price_per_item=product_in_cart.price_per_item,
                                              total_price=product_in_cart.total_price,
                                              order=order)

        if request.user.is_authenticated:
            ProductInCart.objects.filter(user_id=customer.id).delete()
        else:
            ProductInCart.objects.filter(session_key=request.session.session_key).delete()

        def delete_customer():
            if not request.user.is_authenticated:
                customer.delete()
        delete_customer()

    # CARD LOGIC
    def logic_card():
        order = Order.objects.create(user=customer, customer_name=data["username"],
                                     customer_phone=data["phone"],
                                     customer_address=data["address"], status=Order.STATUS_NEW,
                                     payment_method=Order.METHOD_CARD,
                                     comment=data["comment"])

        for name, value in data.items():
            if name.startswith("product_in_cart_"):
                product_in_cart_id = name.split("product_in_cart_")[1]
                product_in_cart = ProductInCart.objects.get(id=product_in_cart_id)
                product_in_cart.nmb = value
                product_in_cart.order = order
                product_in_cart.save(force_update=True)
                ProductInOrder.objects.create(product=product_in_cart.product, nmb=product_in_cart.nmb,
                                              price_per_item=product_in_cart.price_per_item,
                                              total_price=product_in_cart.total_price,
                                              order=order)

        if request.user.is_authenticated:
            ProductInCart.objects.filter(user_id=customer.id).delete()
        else:
            ProductInCart.objects.filter(session_key=request.session.session_key).delete()

        def delete_customer():
            if not request.user.is_authenticated:
                customer.delete()

        request.session['customer_username'] = customer.username
        request.session['order_id'] = order.id
        request.session['customer_id'] = customer.id
        delete_customer()

    args['final_price'] = final_price
    args['user'] = user
    args['form'] = form

    if request.POST:
        if form.is_valid():

            customer = request.user
            data = request.POST

            if request.user.is_authenticated:
                get_user_model().objects.filter(username=request.user.username).update(
                    username=data["username"],
                    phone=data["phone"],
                    address=data["address"])
                customer = get_user_model().objects.get(username=data["username"])
            else:
                customer = get_user_model().objects.create(
                    username=data["username"],
                    phone=data["phone"],
                    address=data["address"])

            if request.POST['customRadio'] == radio_bonuses:
                if logic_bonuses() == "not enough bonuses":
                    args['no_bonuses'] = True
                    return render(request, 'checkout.html', args)
                else:
                    return redirect('orders:thank')
            elif request.POST['customRadio'] == radio_cash:
                logic_cash()
                return redirect('orders:thank')
            elif request.POST['customRadio'] == radio_card:
                logic_card()
                return redirect('orders:card')

    return render(request, 'checkout.html', args)


def card(request):
    if not request.session.get('order_id'):
        raise Http404

    stripe.api_key = settings.STRIPE_PRIVATE_KEY
    # 65000.00

    intent = stripe.PaymentIntent.create(
        amount=int(Order.objects.get(id=request.session['order_id']).total_price * 100),
        currency='uah',
        # Verify your integration in this guide by including this parameter
        metadata={'integration_check': 'accept_a_payment'},)

    client_secret = intent.client_secret

    return render(request, 'card.html', {'client_secret': client_secret, 'intent': intent})


def payed_online(request):
    if request.POST:
        order = Order.objects.get(id=request.session['order_id'])
        order.status = Order.STATUS_PAYED
        order.save()
        if request.user.is_authenticated:
            customer = get_user_model().objects.get(id=request.session['customer_id'])
            customer.bonuses += order.total_price * Decimal("0.01")
            customer.save()

        del request.session['customer_username']
        del request.session['order_id']
        del request.session['customer_id']

        return JsonResponse({"status": "payed"})


class CartView(TemplateView):
    template_name = "cart.html"

    def get_context_data(self, *args, **kwargs):
        context = super(CartView, self).get_context_data(*args, **kwargs)

        return context


def cart_adding(request):
    return_dict = dict()
    session_key = request.session.session_key
    data = request.POST
    product_id = data.get("product_id")
    nmb = data.get("nmb")
    is_delete = data.get("is_delete")

    if is_delete == "true":
        if request.user.is_authenticated:
            ProductInCart.objects.filter(user_id=get_user_model().objects.get(username=request.user.username).id,
                                         is_active=True,
                                         order__isnull=True,
                                         product__id=product_id
                                         ).update(is_active=False)
        else:
            ProductInCart.objects.filter(session_key=session_key,
                                         is_active=True,
                                         order__isnull=True,
                                         product__id=product_id
                                         ).update(is_active=False)

    else:

        def not_created_product_in_cart(count):
            new_product.nmb += int(count)
            new_product.save(force_update=True)

        if request.user.is_authenticated:
            ProductInCart.objects.filter(user_id=get_user_model().objects.get(username=request.user.username).id,
                                         is_active=False,
                                         order__isnull=True
                                         ).delete()

            new_product, created = ProductInCart.objects.get_or_create(
                user_id=get_user_model().objects.get(username=request.user.username).id,
                product_id=product_id, defaults={
                    "session_key": session_key,
                    "user_id": get_user_model().objects.get(username=request.user.username).id,
                    "nmb": nmb
                })
            if not created:
                not_created_product_in_cart(nmb)

        else:
            ProductInCart.objects.filter(session_key=session_key,
                                         is_active=False,
                                         order__isnull=True
                                         ).delete()

            new_product, created = ProductInCart.objects.get_or_create(session_key=session_key,
                                                                       product_id=product_id,
                                                                       defaults={"nmb": nmb})
            if not created:
                not_created_product_in_cart(nmb)

    if request.user.is_authenticated:
        products_in_cart = ProductInCart.objects.filter(
            user_id=get_user_model().objects.get(username=request.user.username).id, is_active=True, order__isnull=True)
    else:
        products_in_cart = ProductInCart.objects.filter(session_key=session_key, is_active=True, order__isnull=True)

    products_total_nmb = 0

    for product_in_cart in products_in_cart:
        products_total_nmb += product_in_cart.nmb

    return_dict["products_total_nmb"] = products_total_nmb
    return_dict["products"] = list()

    for item in products_in_cart:
        product_dict = dict()
        product_dict["name"] = item.product.name
        product_dict["price"] = item.price_per_item
        product_dict["nmb"] = item.nmb
        return_dict["products"].append(product_dict)

    return JsonResponse(return_dict)


def thank(request):
    products = Product.objects.all().order_by('updated').reverse()[:5]

    return render(request, 'thank.html', {'products': products})
