from rest_framework import serializers
from ordersys.models import ProductInCart


class ProductInCartSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductInCart
        fields = "__all__"
