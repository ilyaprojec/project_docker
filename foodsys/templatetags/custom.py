from django import template
import re

register = template.Library()


@register.filter
def uppersplit_filter(var):

    return re.findall('[А-Я][^А-Я]*', var)[1:-1]


@register.filter
def firstword_filter(var):

    return var.split()[0]


@register.filter
def lastword_filter(var):

    listToStr = ' '.join(map(str, var.split()[-2:]))
    return listToStr
