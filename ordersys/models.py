from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save


class ProductStatistic(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True, default=None)
    nmb_in_orders = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Order(models.Model):
    METHOD_CARD = 1
    METHOD_CASH = 2
    METHOD_BONUSES = 3

    METHOD_CHOICES = (
        (METHOD_CARD, 'Payment by Card'),
        (METHOD_CASH, 'Payment by Cash'),
        (METHOD_BONUSES, 'Payment by Bonuses')
    )

    STATUS_NEW = 1
    STATUS_IN_WORK = 2
    STATUS_CANCELED = 3
    STATUS_CLOSED = 4
    STATUS_PAYED = 5

    STATUS_CHOICES = (
        (STATUS_NEW, 'New'),
        (STATUS_IN_WORK, 'In work'),
        (STATUS_CANCELED, 'Canceled'),
        (STATUS_CLOSED, 'Closed'),
        (STATUS_PAYED, 'Payed')
    )

    user = models.ForeignKey(get_user_model(), blank=True, null=True, default=None, on_delete=models.SET_NULL)
    total_price = models.DecimalField(max_digits=10, decimal_places=2, default=0)  # total price for all products in order
    customer_name = models.CharField(max_length=64, blank=True, null=True, default=None)
    customer_email = models.EmailField(blank=True, null=True, default=None)
    customer_address = models.CharField(max_length=255, blank=True, null=True)
    customer_phone = models.CharField(max_length=13, blank=True, null=True)
    comment = models.TextField(blank=True, null=True, default=None)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES)
    payment_method = models.PositiveSmallIntegerField(choices=METHOD_CHOICES)

    def __str__(self):
        return "Заказ %s %s" % (self.id, self.status)

    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'

    def save(self, *args, **kwargs):

        super(Order, self).save(*args, **kwargs)


class ProductInOrder(models.Model):
    order = models.ForeignKey(Order, blank=True, null=True, default=None, on_delete=models.CASCADE)
    product = models.ForeignKey("foodsys.Product", blank=True, null=True, default=None, on_delete=models.CASCADE)
    nmb = models.IntegerField(default=1)
    price_per_item = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    total_price = models.DecimalField(max_digits=10, decimal_places=2, default=0)  # price*nmb
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return "%s" % self.product.name

    class Meta:
        verbose_name = 'Product in Order'
        verbose_name_plural = 'Products in Order'

    def save(self, *args, **kwargs):
        price_per_item = self.product.price
        self.price_per_item = price_per_item
        self.total_price = int(self.nmb) * price_per_item
        created = self.pk is None
        super(ProductInOrder, self).save(*args, **kwargs)
        if created:
            product_statistic, created = ProductStatistic.objects.get_or_create(name=self.product.name, defaults={"name": self.product.name})
            product_statistic.nmb_in_orders += int(self.nmb)
            product_statistic.save(force_update=True)


class ProductInCart(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    session_key = models.CharField(max_length=128, blank=True, null=True, default=None)
    order = models.ForeignKey(Order, blank=True, null=True, default=None, on_delete=models.CASCADE)
    product = models.ForeignKey("foodsys.Product", blank=True, null=True, default=None, on_delete=models.CASCADE)
    nmb = models.IntegerField(default=1)
    price_per_item = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    total_price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return "%s" % self.product.name

    class Meta:
        verbose_name = "Product in Cart"
        verbose_name_plural = "Products in Cart"

    def save(self, *args, **kwargs):
        price_per_item = self.product.price
        self.price_per_item = price_per_item
        self.total_price = int(self.nmb) * price_per_item

        super(ProductInCart, self).save(*args, **kwargs)


def product_in_order_post_save(sender, instance, created, **kwargs):
    order = instance.order
    all_products_in_order = ProductInOrder.objects.filter(order=order, is_active=True)
    order_total_price = 0

    for item in all_products_in_order:
        order_total_price += item.total_price

    instance.order.total_price = order_total_price
    instance.order.save(force_update=True)


post_save.connect(product_in_order_post_save, sender=ProductInOrder)
