FROM python:3.8-slim

WORKDIR /app

COPY . .

RUN cp forms/server_settings.py forms/local_settings.py

RUN pip install -r requirements.txt
