from django.views.generic.base import TemplateView
from foodsys.models import Product
from ordersys.models import ProductStatistic


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, *args, **kwargs):

        main_page_products_nmb = 6
        products = []
        product_statistics = ProductStatistic.objects.all().order_by('nmb_in_orders').reverse()[:main_page_products_nmb]
        for product_statistic in product_statistics:
            products.append(Product.objects.get(name=product_statistic.name))

        if len(product_statistics) < main_page_products_nmb:

            for product in Product.objects.all():
                for stat_prod in products:
                    if stat_prod == product:
                        break
                else:
                    products.append(product)
                    if len(products) == main_page_products_nmb:
                        break

        context = {
            'products': products,
        }

        return context
