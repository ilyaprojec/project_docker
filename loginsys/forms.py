from django import forms
from django.contrib.auth import get_user_model
from django.core.validators import RegexValidator


class LoginForm(forms.Form):
    phone = forms.CharField(max_length=13, required=True, validators=[RegexValidator(r'^\+380\d{9}$', message="Phone number must be entered in the format: '+380XXXXXXXXX'. 12 digits.")])
    password = forms.CharField(max_length=255, widget=forms.PasswordInput)

    def clean_phone(self):
        phone = self.cleaned_data.get('phone')
        if not get_user_model().objects.filter(phone=phone):
            raise forms.ValidationError('User not found')
        return phone

    def clean_password(self, *args, **kwargs):
        phone = self.cleaned_data.get('phone')
        password = self.cleaned_data.get('password')
        if phone and password:
            user = get_user_model().objects.get(phone=phone)
            if not user.check_password(password):
                raise forms.ValidationError("Incorrect password")
        return super(LoginForm, self).clean(*args, **kwargs)


class OrderingContactForm(forms.Form):
    username = forms.CharField(required=True)
    phone = forms.CharField(max_length=13, required=True, validators=[RegexValidator(r'^\+380\d{9}$', message="Phone number must be entered in the format: '+380XXXXXXXXX'. 12 digits.")])
    address = forms.CharField(required=True)

    class Meta:
        model = get_user_model()
        fields = ('phone', 'username', 'address')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(OrderingContactForm, self).__init__(*args, **kwargs)

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if self.user.is_anonymous or self.user.username != username:
            if get_user_model().objects.filter(username=username):
                raise forms.ValidationError(
                    'This username is already in use')
        return username

    def clean_phone(self):
        phone = self.cleaned_data.get('phone')
        if self.user.is_anonymous or self.user.phone != phone:
            if get_user_model().objects.filter(phone=phone):
                raise forms.ValidationError(
                    'This phone is already in use')
        return phone


class PasswordChangeForm(forms.Form):
    old_password = forms.CharField(widget=forms.widgets.PasswordInput())
    password = forms.CharField(widget=forms.widgets.PasswordInput())
    password_confirm = forms.CharField(widget=forms.widgets.PasswordInput())


class RegistrationForm(forms.ModelForm):
    username = forms.CharField(required=True)
    phone = forms.CharField(max_length=13, required=True, initial='+380')
    password_confirm = forms.CharField(widget=forms.widgets.PasswordInput())

    class Meta:
        model = get_user_model()
        fields = ('username', 'phone', 'password', 'password_confirm', )
        widgets = {
            'password': forms.widgets.PasswordInput()
        }

    def clean_password_confirm(self):
        if self.cleaned_data['password_confirm'] != self.cleaned_data['password']:
            raise forms.ValidationError("Passwords do not match")
        return self.cleaned_data['password']

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        user.save()
        return user


class PersonalDataUpdateForm(forms.ModelForm):
    username = forms.CharField(required=True)
    phone = forms.CharField(max_length=13, required=True)
    address = forms.CharField(required=False)

    class Meta:
        model = get_user_model()
        help_texts = {
            'username': None,
        }
        fields = ('username', 'phone', 'address')
