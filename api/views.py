from rest_framework.viewsets import ModelViewSet
from ordersys.models import ProductInCart
from api.serializers import ProductInCartSerializer


class ProductInCartViewSet(ModelViewSet):
    queryset = ProductInCart.objects.all()
    serializer_class = ProductInCartSerializer
