function cartUpdating (product_id, nmb, is_delete) {
    let data = {};
    data.product_id = product_id;
    data.nmb = nmb;
    let csrf_token = $('#csrf_getting_form [name="csrfmiddlewaretoken"]').val();
    data["csrfmiddlewaretoken"] = csrf_token;

    if (is_delete) {
        data["is_delete"] = true;
    }

    let url = '/orders/cart_adding/'
    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        success: function (data) {
            if (data.products_total_nmb || data.products_total_nmb == 0) {
                $('#cart_total_nmb').text("(" + data.products_total_nmb + ")");
            }
        },
    })
}

$('.product-grid').click(function (e) {
    let id = $(this).data("id");
    let path = '/food/product/' + id;
    document.location.href = path;
});

let nav_item = $('.nav-item');
nav_item.each( function() {
    nav_item.removeClass( "active" );
});

$('.nav-item > a[href="'+window.location.pathname+'"]').parent().addClass('active');

$('.profile-menu').each( function() {
    $('.profile-menu').removeClass("btn-outline-success")
});

$('a[href="'+window.location.pathname+'"]').addClass('btn-outline-success')

$('.btn-buy').click(function (e) {
    let product_id = $(this).data("product_id");
    let product_name = $(this).data("name");
    let product_price = $(this).data("price");

    cartUpdating(product_id, nmb=1, is_delete=false);
});

$('.delete-item').click(function (e) {
    e.preventDefault();
    product_id = $(this).data("product_id");
    $(this).closest('tr').remove();

    cartUpdating(product_id, nmb=0, is_delete=true)
    calculatingCartAmount()
});

function calculatingCartAmount() {
    let total_order_amount = 0
    $('.total-product-in-cart-amount').each(function () {
        total_order_amount += parseFloat($(this).text());
    });
    $('#total_order_amount').text(total_order_amount);
    if (total_order_amount == 0) {
        $('#checkout_btn').hide();
    }
};

calculatingCartAmount();
