from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.urls.base import reverse_lazy
from django.views.generic import CreateView, UpdateView
from django.contrib.auth import get_user_model
from django.contrib.auth import update_session_auth_hash
from django.views.generic.base import TemplateView
from django.shortcuts import redirect
from django.contrib import auth
from django.template.context_processors import csrf
from ordersys.models import Order, ProductInOrder
from .forms import RegistrationForm, PasswordChangeForm, PersonalDataUpdateForm, LoginForm


def login(request):
    args = {}
    args.update(csrf(request))
    form = LoginForm(request.POST or None)

    if request.POST:
        if form.is_valid():
            phone = request.POST.get('phone', '')
            password = request.POST.get('password', '')
            user = auth.authenticate(username=phone, password=password)
            if user is not None:
                auth.login(request, user)
                return redirect('/')

    args['form'] = form

    return render(request, 'login.html', args)


def logout(request):
    auth.logout(request)
    return redirect("main:home")


class RegistrationView(CreateView):
    model = get_user_model()
    form_class = RegistrationForm
    template_name = 'registration.html'
    success_url = reverse_lazy('main:home')


class UserUpdateView(UpdateView):
    template_name = 'update_user.html'

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return super(UserUpdateView, self).dispatch(*args, **kwargs)
        else:
            HttpResponse(status_code=404)

    success_url = reverse_lazy('loginsys:profile-personal-data')
    model = get_user_model()
    form_class = PersonalDataUpdateForm
    slug_field = "username"
    slug_url_kwarg = "username"


def profile_change_password(request):
    if not request.user.is_authenticated:
        raise Http404

    args = {}
    args.update(csrf(request))
    form = PasswordChangeForm()
    user = get_user_model().objects.get(username=auth.get_user(request).username)

    if request.method == 'POST':
        password = request.POST.get('password')
        password_confirm = request.POST.get('password_confirm')
        old_password = request.POST.get('old_password')
        if user.check_password(old_password):
            if password == password_confirm:
                user.set_password(password)
                user.save()
                update_session_auth_hash(request, user)
                args['password_error'] = "Ok"
                args['ok'] = 'Ok'
            else:
                args['password_error'] = "Passwords do not match"
        else:
            args['password_error'] = "Incorrect old password"
    return render(request, 'profile_password_change.html', context={'args': args, 'form': form})


class ProfileView(TemplateView):
    template_name = "profile.html"

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return super(ProfileView, self).dispatch(*args, **kwargs)
        else:
            HttpResponse(status_code=404)

    def get_context_data(self, *args, **kwargs):
        context = super(ProfileView, self).get_context_data(*args, **kwargs)
        context['username'] = auth.get_user(self.request).username
        context['bonuses'] = get_user_model().objects.get(username=auth.get_user(self.request).username).bonuses
        user_orders = Order.objects.filter(user__username=context['username'])
        orders = []
        for user_order in user_orders:
            order = []
            products_in_order = ProductInOrder.objects.filter(order=user_order)
            order.append(user_order)
            order.append(products_in_order)
            orders.append(order)
        if orders:
            context['orders'] = reversed(orders)
        return context


def profile_personal_data(request):
    if not request.user.is_authenticated:
        raise Http404

    user = get_user_model().objects.filter(username=auth.get_user(request).username)
    return render(request, 'profile_personal_data.html', locals())
